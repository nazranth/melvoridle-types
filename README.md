# Melvor Idle Types

Type definitions for Melvor Idle.

I will try to keep this as updated as I can, feel free to submit merge requests with updates or poke me if this needs to be updated.

### Install
```bash
npm install https://gitlab.com:nazranth/melvoridle-types
```

### Development notes
 Class methods must be declared as **static**
