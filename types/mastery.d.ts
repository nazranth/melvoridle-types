declare function masteryExp(): void;
declare class masteryExp {
    static equate: (xp: any) => number;
    static level_to_xp: (level: any) => number;
    static xp_to_level: (xp: any) => number;
}
declare function loadMasteryTab(): void;
declare function updateMasteryLevel(skill: any, id: any, masteryArray: any): void;
declare function levelUpMastery(skill: any, id: any, masteryArray: any, token?: boolean, notify?: boolean): void;
declare function updateMasteryProgress(skill: any, masteryID: any, masteryArray: any): void;
declare function getMasteryProgress(masteryID: any, masteryArray: any): number;
declare function addMasteryXP(skill: any, masteryID: any, qty?: number): void;
declare function updateMasteryArrays(): void;
declare function updateCurrentMastery(skill: any, masteryArray: any): void;
declare function showMasteryProgress(skill: any, masteryArray: any): void;
declare function getMasteryMedia(masteryID: any, skill: any): string;
declare function showMasteryUnlocks(skill: any): void;
declare function repairMastery(): void;
declare var currentMastery: any[];
declare var totalMastery: any[];
declare const na: "t";
declare var masteryMedia: {
    0: {
        media: string;
    }[];
    1: {
        media: string;
    }[];
    2: {
        media: string;
    }[];
    3: {
        media: string;
    }[];
    4: {
        media: string;
    }[];
    5: {
        media: string;
    }[];
    10: {
        media: string;
    }[];
    11: {
        media: string;
    }[];
};
declare var masteryUnlocks: {
    0: {
        level: number;
        unlock: string;
    }[];
    1: {
        level: number;
        unlock: string;
    }[];
    2: {
        level: number;
        unlock: string;
    }[];
    3: {
        level: number;
        unlock: string;
    }[];
    4: {
        level: number;
        unlock: string;
    }[];
    5: {
        level: number;
        unlock: string;
    }[];
    10: {
        level: number;
        unlock: string;
    }[];
    11: {
        level: number;
        unlock: string;
    }[];
    13: {
        level: number;
        unlock: string;
    }[];
    14: {
        level: number;
        unlock: string;
    }[];
    15: {
        level: number;
        unlock: string;
    }[];
    19: {
        level: number;
        unlock: string;
    }[];
};
declare var treeMasteryData: {
    mastery: number;
    masteryXP: number;
}[];
declare var fishMastery: {
    mastery: number;
    masteryXP: number;
}[];
declare var logsMastery: {
    mastery: number;
    masteryXP: number;
}[];
declare var cookingMastery: {
    mastery: number;
    masteryXP: number;
}[];
declare var miningOreMastery: {
    mastery: number;
    masteryXP: number;
}[];
declare var smithingMastery: {
    mastery: number;
    masteryXP: number;
}[];
declare var thievingMastery: {
    mastery: number;
    masteryXP: number;
}[];
declare var farmingMastery: {
    mastery: number;
    masteryXP: number;
}[];
declare var fletchingMastery: any[];
declare var craftingMastery: any[];
declare var runecraftingMastery: any[];
declare var herbloreMastery: any[];
