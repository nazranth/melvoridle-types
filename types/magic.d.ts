declare function castMagic(clicked?: boolean): void;
declare function selectMagic(altMagicID: any, update?: boolean): void;
declare function selectItemForMagic(id: any, selectedItem?: any, select?: boolean): void;
declare function setMagicItemImg(itemID: any): void;
declare const SPELLS: ({
    name: string;
    media: string;
    magicLevelRequired: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    maxHit: number;
    spellType: any;
    runesRequiredAlt?: undefined;
} | {
    name: string;
    media: string;
    magicLevelRequired: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    maxHit: number;
    spellType: any;
})[];
declare const CURSES: ({
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    chance: number;
    effectValue: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    chance: number;
    effectValue: number[];
    runesRequired: {
        id: any;
        qty: number;
    }[];
})[];
declare const AURORAS: ({
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    effectValue: number[];
    runesRequired: {
        id: any;
        qty: number;
    }[];
    requiredItem: any;
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    effectValue: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    requiredItem: any;
})[];
declare const ANCIENT: {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    specID: number;
    maxHit: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    requiredDungeonCompletion: any[];
}[];
declare const ALTMAGIC: ({
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    selectItem: number;
    convertTo: any;
    convertToQty: number;
    magicXP: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    effectValue?: undefined;
    isAlch?: undefined;
    ignoreCoal?: undefined;
    isJunk?: undefined;
    needCoal?: undefined;
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    selectItem: number;
    convertToQty: number;
    magicXP: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    convertTo?: undefined;
    effectValue?: undefined;
    isAlch?: undefined;
    ignoreCoal?: undefined;
    isJunk?: undefined;
    needCoal?: undefined;
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    effectValue: number;
    selectItem: number;
    isAlch: boolean;
    ignoreCoal: boolean;
    magicXP: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    convertTo?: undefined;
    convertToQty?: undefined;
    isJunk?: undefined;
    needCoal?: undefined;
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    selectItem: number;
    convertToQty: number;
    isJunk: boolean;
    magicXP: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    convertTo?: undefined;
    effectValue?: undefined;
    isAlch?: undefined;
    ignoreCoal?: undefined;
    needCoal?: undefined;
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    effectValue: number;
    convertTo: any;
    convertToQty: number;
    selectItem: number;
    magicXP: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    isAlch?: undefined;
    ignoreCoal?: undefined;
    isJunk?: undefined;
    needCoal?: undefined;
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    selectItem: number;
    convertToQty: number;
    ignoreCoal: boolean;
    magicXP: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    convertTo?: undefined;
    effectValue?: undefined;
    isAlch?: undefined;
    isJunk?: undefined;
    needCoal?: undefined;
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    selectItem: number;
    convertToQty: number;
    needCoal: boolean;
    magicXP: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    convertTo?: undefined;
    effectValue?: undefined;
    isAlch?: undefined;
    ignoreCoal?: undefined;
    isJunk?: undefined;
} | {
    name: string;
    media: string;
    description: string;
    magicLevelRequired: number;
    effectValue: number;
    selectItem: number;
    isAlch: boolean;
    magicXP: number;
    runesRequired: {
        id: any;
        qty: number;
    }[];
    runesRequiredAlt: {
        id: any;
        qty: number;
    }[];
    convertTo?: undefined;
    convertToQty?: undefined;
    ignoreCoal?: undefined;
    isJunk?: undefined;
    needCoal?: undefined;
})[];
declare var isMagic: boolean;
declare var selectedAltMagic: any;
declare var currentMagicSpell: any;
declare var selectedMagicItem: any[];
declare var magicInterval: number;
declare var magicTimeout: any;
