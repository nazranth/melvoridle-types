declare function cutTree(treeID: any, ignore: any): void;
declare function updateWCMilestone(notify: any): void;
declare function updateWCRates(): void;
declare const trees: {
    type: string;
    level: number;
    interval: number;
    xp: number;
    media: string;
}[];
declare var axeBonusSpeed: number[];
declare var treeCutLimit: number;
declare var currentlyCutting: number;
declare var currentTrees: any[];
declare var treeCuttingHandler: any[];
declare var treeAutoHandler: boolean[];
