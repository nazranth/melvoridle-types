declare const PRAYER: {
    name: string;
    description: string;
    prayerLevel: number;
    media: string;
    vars: string[];
    values: number[];
    pointsPerPlayer: number;
    pointsPerEnemy: number;
    pointsPerRegen: number;
}[];
