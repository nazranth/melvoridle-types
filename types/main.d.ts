declare function updateWindow(cloudSave?: boolean): void;
declare function updateGP(): void;
declare function idleChecker(currentSkill: any): boolean;
declare function updateSkillWindow(skill: any): void;
declare function updateLevelProgress(skill: any): void;
declare function levelUp(skill: any): void;
declare class exp {
    static equate: (xp: any) => number;
    static level_to_xp: (level: any) => number;
    static xp_to_level: (xp: any) => number;
}
declare function convertGP(currentGP: any): any;
declare function formatNumber(number: any): any;
declare function numberWithCommas(x: any): any;
declare function changePage(page: any, gameLoading?: boolean, toggleSidebar?: boolean): void;
declare function setToUppercase(string: any): any;
declare function setToLowercase(string: any): any;
declare function updateTooltips(): void;
declare function masteryNotify(mastery: any, skill: any, level: any): void;
declare function itemNotify(itemID: any, qty: any): void;
declare function gpNotify(qty: any): void;
declare function stunNotify(damage: any): void;
declare function bankFullNotify(): void;
declare function notifyBirdNest(message: any, type: any): void;
declare function notifyGemRock(message: any): void;
declare function levelUpNotify(skill: any): void;
declare function notifyPlayer(skill: any, message: any, type?: string): void;
declare function notifyGloves(skill: any): void;
declare function notifySlayer(qty: any, type?: string): void;
declare function updateMilestoneTab(skill: any): void;
declare function selectFromDropTable(itemID: any): any;
declare function selectFromLootTable(skill: any, id: any): any;
declare function updateGloves(gloves: any, skill: any): void;
declare function addXP(skill: any, xp: any): void;
declare function addXPBonuses(skill: any, xp: any): number;
declare function getMasteryToken(skill: any, offline?: boolean): number;
declare function timestamp(achievement: any, skill?: any, itemID?: any): void;
declare function gloveCheck(): void;
declare function loadProfile(): void;
declare function httpsCheck(): void;
declare function toggleMenu(menu: any): void;
declare function updateOffline(continueAction?: boolean): void;
declare function clearOffline(): void;
declare function offlineCatchup(): void;
declare function toggleCombatSkillMenu(): void;
declare function dropRingHalfA(levelReq: any, offline?: boolean): number;
declare function activateTutorialTip(tipID: any): void;
declare function resetTutotialTips(): void;
declare function updateSaveFileChanges(): void;
declare function pauseOfflineAction(skill: any): void;
declare function getItemMedia(itemID: any): any;
declare function initMinibar(): void;
declare function updateMinibar(pageName: any): void;
declare function toggleSkillMinibar(): void;
declare function quickEquipSkillcape(skill: any): void;
declare function quickEquipItem(item: any, skill: any): void;
declare function setGamemode(gamemode: any): void;
declare function loadCharacterSelection(): void;
declare function processLocalCharacters(): void;
declare function processCloudCharacters(charID: any, save: any): void;
declare function resetCharacterSelection(charID: any): void;
declare function toggleCharacterSelectionView(): void;
declare function selectCharacter(char: any, confirmed?: boolean): void;
declare function resetAccountData(): void;
declare function rollForRhaelyx(skill: any, offline?: boolean): any;
declare function checkRhaelyx(): boolean;
declare function removeChargeRhaelyx(): void;
declare function initTooltips(): void;
declare function finaliseLoad(): void;
declare function setupNewCharacter(): void;
declare const SKILLS: {
    0: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    1: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    2: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    3: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    4: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    5: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    6: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    7: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    8: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    9: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    10: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    11: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    12: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    13: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    14: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    15: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    16: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    17: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    18: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
    19: {
        name: string;
        media: string;
        hasMastery: boolean;
        maxLevel: number;
    };
};
declare const pages: string[];
declare const specialEvents: {
    active: boolean;
}[];
declare var el: HTMLElement;
declare var sortable: any;
declare const gameTitle: "Melvor Idle :: Alpha v0.16.2";
declare var gp: number;
declare var slayerCoins: number;
declare var currentBank: number;
declare var currentPage: number;
declare var announcementID: string;
declare const us: "w";
declare const p: 848;
declare var saveTimestamp: number;
declare var mapLoaded: boolean;
declare var itemLog: any[];
declare var itemStats: any[];
declare var monsterStats: any[];
declare var isLoaded: boolean;
declare let confirmedLoaded: boolean;
declare var currentlyCatchingUp: boolean;
declare var killCount: any[];
declare const tutorialT: 168;
declare const ar: number[];
declare namespace offline {
    const skill: any;
    const action: any;
    const timestamp: any;
}
declare const IItemID: number;
declare var skillsMenu: boolean;
declare var combatMenu: boolean;
declare var easterLoaded: boolean;
declare var offlinePause: boolean;
declare var currentGamemode: number;
declare var tutorialTipData: {
    id: number;
    title: string;
    titleImg: string;
    description: string;
}[];
declare var tutorialTips: {
    activated: boolean;
}[];
declare namespace tooltipInstances {
    const bank: any[];
    const spellbook: any[];
    const equipment: any[];
    const minibar: any[];
    const combatInfo: any[];
    const specialAttack: any[];
    const equipmentSets: any[];
}
declare function arrSum(arr: any): any;
declare var eightSeconds: boolean;
declare var currentView: number;
declare var lolYouDiedGetRekt: boolean;
declare var itemLogBuilt: boolean;
declare var allVars: string[];
declare var skillName: string[];
declare var skillXP: number[];
declare var skillLevel: number[];
declare var nextLevelProgress: number[];
