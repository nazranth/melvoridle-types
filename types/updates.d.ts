declare function createChangelog(): void;
declare const updateNew: "<span class=\"badge badge-pill badge-success\">NEW</span> ";
declare const updateChange: "<span class=\"badge badge-pill badge-primary\">CHANGE</span> ";
declare const updateFix: "<span class=\"badge badge-pill badge-warning\">FIXED</span> ";
declare const updateRemoved: "<span class=\"badge badge-pill badge-danger\">REMOVED</span> ";
declare const fontStyle: "<span class=\"font-size-sm text-muted\">";
