declare function updateStats(skill?: any): void;
declare function updateItemLog(item?: any, quantity?: number): void;
declare function openItemLog(): void;
declare function openMonsterLog(): void;
declare function openPetLog(): void;
declare function updateCompletionLog(): void;
declare var statsGeneral: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsCombat: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsWoodcutting: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsFishing: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsFiremaking: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsCooking: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsMining: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsSmithing: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsThieving: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsFarming: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsFletching: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsCrafting: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsRunecrafting: {
    stat: string;
    id: string;
    count: number;
}[];
declare var statsHerblore: {
    stat: string;
    id: string;
    count: number;
}[];
