declare function startHerblore(clicked?: boolean): void;
declare function loadHerblore(update?: boolean): void;
declare function selectHerblore(herbloreID: any, update?: boolean): void;
declare function getHerbloreQty(itemID: any): any;
declare function updateHerbloreQty(itemID: any): void;
declare function getHerbloreTier(id: any): number;
declare function checkHerbloreReq(itemID: any): boolean;
declare function herbloreCategory(cat: any): void;
declare function loadPotions(): void;
declare function usePotion(itemID: any): void;
declare function updateHerbloreBonuses(itemID: any, qty?: number, use?: boolean): void;
declare function updatePotionHeader(): void;
declare function getHerbloreMasteryID(itemID: any, tier: any): number | false;
declare const herbloreItemData: {
    id: number;
    name: string;
    itemID: any[];
    category: number;
    herbloreLevel: number;
    herbloreXP: number;
}[];
declare var herbloreItems: any[];
declare var isHerblore: boolean;
declare var herbloreTimeout: any;
declare var herbloreReqCheck: any[];
declare var selectedHerblore: any;
declare var currentHerblore: any;
declare var herbloreInterval: number;
declare const masteryTiers: number[];
declare var herbloreBonuses: ({
    itemID: number;
    bonus: any[];
    charges: number;
} | {
    itemID?: undefined;
    bonus?: undefined;
    charges?: undefined;
})[];
