declare function loadPets(): void;
declare function updatePet(petID: any): void;
declare function rollForPet(petID: any, timePerAction: any, offline?: boolean): boolean;
declare function unlockPet(petID: any): void;
declare function updatePetSidebar(): void;
declare function simulateRollForPet(petID: any, timePerAction: any, qty: any, chanceValue: any): void;
declare const PETS: ({
    name: string;
    description: string;
    media: string;
    acquiredBy: string;
    skill: any;
    chance: number;
    killCount?: undefined;
} | {
    name: string;
    description: string;
    media: string;
    acquiredBy: string;
    skill: any;
    chance?: undefined;
    killCount?: undefined;
} | {
    name: string;
    description: string;
    media: string;
    acquiredBy: string;
    killCount: number;
    skill?: undefined;
    chance?: undefined;
})[];
declare var petUnlocked: any[];
