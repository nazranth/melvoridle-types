declare function bankUpgradeCost(): void;
declare class bankUpgradeCost {
    static equate: (gp: any) => number;
    static level_to_gp: (level: any) => number;
    static gp_to_level: (gp: any) => number;
}
declare function newNewBankUpgradeCost(): void;
declare class newNewBankUpgradeCost {
    static equate: (level: any) => number;
    static level_to_gp: (level: any) => any;
}
declare function newBankUpgradeCost(): void;
declare class newBankUpgradeCost {
    static equate: (gp: any) => number;
    static level_to_gp: (level: any) => number;
    static gp_to_level: (gp: any) => number;
}
declare function updateShop(identifier: any): void;
declare function updateBuyQty(qty: any): void;
declare function updateShopPrices(): void;
declare function upgradeBank(confirmed?: boolean): void;
declare function upgradeAxe(confirmed?: boolean): void;
declare function upgradeRod(confirmed?: boolean): void;
declare function upgradePickaxe(confirmed?: boolean): void;
declare function upgradeMultiTree(confirmed?: boolean): void;
declare function upgradeCookingFire(confirmed?: boolean): void;
declare function buyBowstring(confirmed?: boolean): void;
declare function buyLeather(confirmed?: boolean): void;
declare function buyDhide(dhide: any, confirmed?: boolean): void;
declare function buyCompost(confirmed?: boolean): void;
declare function buyFeathers(confirmed?: boolean): void;
declare function buyPartyHat(confirmed?: boolean): void;
declare function buyGloves(gloves: any, confirmed?: boolean): void;
declare function buySkillcape(cape: any, confirmed?: boolean): void;
declare function loadSlayerShop(): void;
declare function buySlayerItem(itemID: any, confirmed?: boolean): void;
declare function upgradeAutoEat(confirmed?: boolean): void;
declare function upgradeEquipmentSet(set: any, confirmed?: boolean): void;
declare function toggleShopMenu(menu: any): void;
declare function buyItem(itemID: any, confirmed?: boolean): void;
declare function upgradeEquipmentSwap(confirmed?: boolean): void;
declare function buyGodUpgrade(id: any, confirmed?: boolean): void;
declare function shopCategory(cat?: any): void;
declare const tiers: string[];
declare const axeLevels: number[];
declare const axeCost: number[];
declare const rodLevels: number[];
declare const rodCost: number[];
declare const pickaxeLevels: number[];
declare const pickaxeCost: number[];
declare var currentAxe: number;
declare var currentRod: number;
declare var currentPickaxe: number;
declare var cookingRangeCost: number;
declare var currentCookingFire: number;
declare var currentAutoEat: number;
declare var buyQty: number;
declare const multiTreeCost: number[];
declare const pickaxeBonus: number[];
declare const pickaxeBonusSpeed: number[];
declare const rodBonusSpeed: number[];
declare const autoEatData: {
    title: string;
    eatAt: number;
    maxHP: number;
    efficiency: number;
    cost: number;
}[];
declare const equipmentSetData: {
    title: string;
    cost: number;
}[];
declare const equipmentSwapData: {
    title: string;
    desc: string;
    cost: number;
}[];
declare var equipmentSetsPurchased: boolean[];
declare var equipmentSwapPurchased: boolean;
declare const dhideCost: {
    tier: string;
    itemID: any;
    costGP: number;
    costLeather: any[];
}[];
declare const cookingFireData: {
    tier: string;
    fmLevel: number;
    costGP: number;
    costLogs: any[];
    bonusXP: number;
    media: string;
}[];
declare const skillcapeItems: any[];
declare const slayerItems: any[];
declare const glovesCost: number[];
declare const glovesActions: number[];
declare const gloveID: any[];
declare var glovesTracker: {
    name: string;
    isActive: boolean;
    remainingActions: number;
}[];
declare const godUpgradeData: {
    name: string;
    description: string;
    cost: number;
    dungeonID: any;
    media: string;
}[];
declare var godUpgrade: boolean[];
