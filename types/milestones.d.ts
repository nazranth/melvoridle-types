declare namespace MILESTONES {
    const Woodcutting: ({
        level: number;
        name: string;
        media: string;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Fishing: ({
        level: number;
        name: string;
        media: string;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Firemaking: ({
        level: number;
        name: string;
        media: string;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Cooking: ({
        level: number;
        name: string;
        media: string;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Mining: {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    }[];
    const Smithing: {
        name: string;
        media: string;
        level: number;
    }[];
    const Attack: ({
        level: number;
        name: string;
        media: string;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Strength: {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    }[];
    const Defence: ({
        level: number;
        name: string;
        media: string;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Hitpoints: {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    }[];
    const Thieving: ({
        name: string;
        level: number;
        media: string;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Farming: ({
        name: string;
        media: string;
        level: number;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Ranged: ({
        name: string;
        media: string;
        level: number;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Fletching: ({
        name: string;
        media: string;
        level: number;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Crafting: {
        name: string;
        media: string;
        level: number;
    }[];
    const Runecrafting: {
        level: number;
        media: string;
        name: string;
    }[];
    const Magic: ({
        level: number;
        name: string;
        media: string;
        alwaysShow?: undefined;
    } | {
        level: number;
        name: string;
        media: string;
        alwaysShow: boolean;
    })[];
    const Prayer: {
        level: number;
        name: string;
        media: string;
    }[];
    const Slayer: {
        level: number;
        name: string;
        media: string;
    }[];
    const Herblore: {
        level: number;
        name: string;
        media: string;
    }[];
}
