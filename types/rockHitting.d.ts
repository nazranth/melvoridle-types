declare function mineRock(ore: any, clicked?: boolean, ignoreDepletion?: boolean): void;
declare function loadMiningOres(): void;
declare function updateMiningOres(): void;
declare function updateRockHP(ore: any, initialise?: boolean): void;
declare function rockReset(ore: any): void;
declare function canMineDragonite(): boolean;
declare function collectGem(offline?: boolean): any;
declare function updateMiningRates(): void;
declare const miningData: {
    level: number;
    respawnInterval: number;
    ore: any;
    masteryID: number;
}[];
declare var rockData: {
    maxHP: number;
    damage: number;
    depleted: boolean;
    respawnTimer: any;
}[];
declare const oreTypes: string[];
declare var miningItemID: any[];
declare var isMining: boolean;
declare var currentRock: any;
declare var miningTimeout: any;
declare const baseMiningInterval: 3000;
declare const gemSpawnInterval: 60000;
declare const gemXP: 500;
declare const baseRockHP: 5;
